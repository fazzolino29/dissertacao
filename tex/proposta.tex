\section{Proposta}

A proposta deste trabalho é unir as vantagens presentes em abordagens caixa-preta às vantagens presentes em abordagens caixa-branca de teste de software,
de tal forma que o esforço de teste seja minimizado ao máximo, sem afetar a qualidade do produto entregue. Abordagens caixa-branca
são focadas na verificação do sistema, enquanto abordagens caixa-preta são focadas na validação do mesmo, como mostra ~\cite{nidhra2012black}.
Desse modo, a utilização das duas abordagens costuma se complementar durante o processo de desenvolvimento, buscando a adequação
à etapa de Verificação e Validação de software.
Sabe-se que, durante o processo de desenvolvimento, algumas abordagens de teste são priorizadas, fazendo com que os casos de teste daquela abordagem
sejam implementados com maior antecedência em relação a outras abordagens de teste. Entretanto, de acordo com ~\cite{henard2016comparing},
em relação a abordagens caixa-preta ou caixa-branca, a priorização de uma delas não envolve um resultado tão distinto, quando se
refere a taxa de identificação de falhas por número de casos de teste implementados. De acordo com o estudo, a diferença
entre a taxa de identificação de falhas média, na priorização de uma das duas abordagens, não ultrapassa o valor
de 4\% de falhas.

A partir deste contexto, o presente trabalho busca definir uma estratégia de teste que relacione as duas abordagens
de tal forma que o esforço necessário para alcançar a confiabilidade desejada seja reduzido, em comparação com
abordagens tradicionais de teste. Com este objetivo, foi selecionado o conceito de \textit{Behavior Driven Development} (BDD) englobando a
abordagem caixa-preta, reunindo, ainda, características importantes, como a definição de requisitos em \textit{Features}
detalhadas, seguindo a estratégia de história de usuário \cite{smart2014bdd}.
Ainda em relação a abordagem caixa-preta, a abordagem proposta é baseada no conceito de \textit{Operational Profile}, apresentado por Musa \cite{musa1996operational}.

Já em relação a abordagem caixa-branca, utiliza-se, neste trabalho, o conceito de \textit{Program Spectrum},
o qual apresenta o registro de execução de cada entidade do código, como declarações de classes, métodos e \textit{branches} \cite{harrold1998empirical}.
Com esta informação, temos uma visão clara e detalhada do número de vezes que cada entidade do código é exercitada pelo conjunto de casos de teste
existentes, servindo como um \textit{input} para o processo de priorização de novos casos de teste, como sugerido por \cite{bertolino2017adaptive}.
Na abordagem \textit{Feature Trace} o \textit{Count Spectra} do sistema faz referência a execução dos cenários BDD existentes, que compõem o conjunto
de requisitos do software.

Assim como a abordagem \textit{Covrel}, apresentada por \cite{bertolino2017adaptive}, o processo de teste proposto neste trabalho
leva em consideração cada ciclo de execução, analisando dinamicamente o \textit{Program Spectrum} do software. Esta análise dinâmica
possibilita que o processo de teste se adapte de acordo com a implementação de novos casos de teste.

A seção \ref{sec:rel_op_bdd} apresenta a relação entre os conceitos de Perfil Operacional e \textit{Behavior Driven Development},
possibilitanto a adoção das duas abordagens de maneira complementar.

\subsection{\textit{Operational Profile} from \textit{Behavior Driven Development}}
\label{sec:rel_op_bdd}
De acordo com \cite{musa1996operational}, o Perfil Operacional do sistema apresenta, de maneira quantitativa, como o sistema será
utilizado no seu ambiente de produção. Este conceito é dividido em 5 camadasa, como o apresentado na Figura \ref{img:op}:

\begin{figure}[htb!]
\centering
\includegraphics[width=0.7\columnwidth]{img/op_structure}
\caption{Níveis do Perfil Operacional.}
\label{img:op}
\end{figure}

Como sabemos, a abordagem BDD é baseada na definição de \textit{features} e \textit{scenarios} de uso do sistema. Desse modo, este
trabalho busca relacionar o conceito de \textit{feature} com o conceito de \textbf{Functional Profile} presente
na técnica de \textit{Operational Profile}. O conceito de \textbf{User Profile} pode ser obtido a partir da utilização das
\textit{tags} presentes na \textit{feature} BDD e o \textit{System-mode Profile} pode ser obtido a partir do conceito de
\textit{Background}, na abordagem BDD. Em relação a menor granularidade da abordagem, o nível de ``Perfil Operacional'' do sistema,
pode ser relacionado com o \textit{path} de execução dos \textit{scenarios} BDD. A Tabela \ref{tab:concepts_relation} apresenta,
de forma clara, a relação entre cada conceito nas duas abordagens.

\begin{table}[H]
	\centering
    \label{tab:concepts_relation}
    \caption{Operational Profile entities from BDD concepts. }
\begin{tabular}{cc}
\textbf{\begin{tabular}[c]{@{}c@{}}Operational Profile \\ Level\end{tabular}} & \textbf{\begin{tabular}[c]{@{}c@{}}Can be \\ obtained in BDD \\from\end{tabular}} \\ \hline
Customer Profile                                                              & \textcolor{red}{Cannot be obtained}                                                      \\\\
User Profile                                                                  & \textcolor{darkgreen}{\texttt{\@<User tag>}}                                                   \\\\
System-mode Profile                                                           & \textcolor{darkgreen}{\texttt{Background}}                                                               \\\\
Functional Profile                                                            & \textcolor{darkgreen}{\texttt{Feature}}                                                                  \\ \\
Operational Profile                                                           & \begin{tabular}[c]{@{}c@{}}\textcolor{darkgreen}{Scenarios traceability} \\ \end{tabular}     \\ \hline
\end{tabular}
\end{table}

Observa-se a inviabilidade da obtenção do conceito de \textbf{Customer Profile}, o que pode prejudicar a qualidade do Perfil Operacional
resultante. Entretanto, assim como o apresentado por \cite{bertolino2017adaptive}, a utilização do conceito de \textit{Program Spectrum}
faz com que o impacto negativo de um Perfil Operacional mal detalhado seja reduzido, dado que o critério de priorização dos casos de teste
utiliza, além do Perfil Operacional, o valor de \textit{Count Spectra}.

A relação entre as duas abordagens possibilita a obtenção de cada unidade conceitual, entretanto, o \textit{input} de um expert do domínio
é necessário para a inclusão das probabilidades de execução de cada \textit{User Tag}, \textit{Background}, \textit{feature} e \textit{cenários} BDD. Com os valores de probabilidade inseridos,
é possível distribuir as probabilidades de execução entre os as entidades de baixo nível, alcançando o conceito de \textit{Perfil Operacional}.


Essa obtenção das probabilidade de ocorrência de cada nível do \textit{Operational Profile}, exceto do nível mais baixo, deverá ser
realizada via interação com os \textit{stakeholders} do sistema. Após a inclusão de todas as probabilidades de ocorrência, será possível
identificar as unidades computacionais que merecem maior atenção no processo de verificação e validação do software.
Com esta informação, o conceito de Perfil Operacional é alcançado, o qual, no trabalho de \cite{bertolino2017adaptive}, é dado como uma
premissa para utilização da abordagem \textit{Covrel}. Em relação ao conceito de \textit{Count Spectra}, a abordagem \textit{Feature Trace}
registra a execução de cada entidade do código, relacionando-a a determinado cenário de uso do software. Com a distribuição destas relações,
é possível gerar uma visualização de alto nível do software, apresentando todas as entidades do código que compõem a execução de determinado
cenário de uso. A Figura \ref{img:grafo_feature} apresenta um exemplo da visualização de alto nível de uma \textit{Feature} chamada
\textit{Account Notification}, considerando um software exemplo que implementa uma rede social.

\begin{figure}[htb!]
\centering
\includegraphics[width=0.7\columnwidth]{img/entityMap}
\caption{Visualização de alto nível de uma Feature BDD.}
\label{img:grafo_feature}
\end{figure}

A obtenção do \textit{Count Spectra} é feita a partir da contagem de quantas arestas cada método possui, levando em consideração todos
os \textit{cenários} de todas as \textit{features} presentes no software. Em relação ao exemplo apresentado na Figura \ref{img:grafo_feature}, por exemplo,
o método \textbf{create\_text\_notification} possui um valor de \textit{Count Spectra} igual a três.

é possível iniciar o processo de teste unitário do sistema, ponderando o nível de importância
de cada entidade presente no sistema. Além do grau de importância, deve-se levar em consideração o número de vezes que cada
entidade foi exercitada pelo conjunto de casos de teste existente (\textit{program spectrum}). Esta informação faz com que a
estratégia de teste continue eficiente (em relação à técnica de \textit{Operational Profile}) após alguns ciclos de teste,
mesmo após a execução de todas as entidades consideradas importantes no sistema, como mostra \cite{bertolino2017adaptive}.

A partir da implementação dos novos casos de teste, o \textit{program spectrum} continua sendo registrado e utilizado para definição
do próximo ciclo de teste, fazendo com que o processo de teste seja um processo interativo, incremental e adaptativo. O critério de parada
utilizado é definido pela equipe de teste, levando em consideração o custo e o prazo do projeto. Ou seja,
a estratégia proposta busca alcançar a maior confiabilidade possível com a quantidade de casos de teste viável no contexto do projeto.

\subsection{Contexto Adequado}

A utilização da abordagem proposta merece uma análise do contexto em que se deseja aplicar, buscando evitar um esforço desnecessário.
De acordo com (levantar autores que questionam a utilização de BDD no front, já vi alguns.), a utilização da abordagem BDD
aplicada ao \textit{front-end} pode gerar um retrabalho considerável por mudanças de interface, além de tornar a execução lenta
e sua implementação complexa.

A sugestão é que a estratégia apresentada neste trabalho seja aplicada no \textit{back-end} do sistema em análise. Além disso,
espera-se que a arquitetura utilizada tenha sido planejada de acordo com as características de testabilidade levantadas por (lembrar do artigo que fala disso.),
possibilitando que a abordagem seja eficiente e agregue valor ao processo de desenvolvimento. A aplicação da abordagem BDD sobre
as interfaces (principalmente no \textit{front-end}) pode gerar um retrabalho considerável, já que toda interface está sujeita a
alterações ao longo do processo de desenvolvimento. Desse modo, é sugerida a utilização da abordagem imersa no código, assim como
a implementação de testes unitários e de integração, sem exercitar a interface.


\section{Processo e Facilitadores}

Com o objetivo de facilitar o processo de teste proposto neste trabalho, vem sendo implementada uma ferramenta de apoio ao processo,
possibilitando a interação entre a equipe de testes, o usuário final e o sistema em análise. O principal objetivo da ferramenta
é automatizar atividades como a distribuição das probabilidades em todos os níveis do \textit{Operational Profile}, assim como
executar o conjunto de testes disponíveis e registrar o \textit{program spectrum}, possibilitando a seleção das entidades que mais
impactam na confiabilidade do sistema.

Com a utilização da ferramenta proposta, cada perfil de usuário do projeto pode interagir paralelamente incluindo informações úteis ao sistema,
ou seja, incluindo o Perfil Operacional referente ao seu \textit{User Profile}. Para que o preenchimento seja realizado de acordo
com as \textit{features} já implementadas, a ferramenta fará uma extração de todas as \textit{features} e \textit{scenarios} do sistema,
apresentando ao usuário para que o mesmo possa incluir as probabilidades de ocorrência.

A Figura \ref{img:op_bdd} apresenta o processo de execução do ciclo de teste proposto neste trabalho. O processo destaca a interação
realizada por parte da \textit{Ferramenta} e por parte do \textit{Testador/Expert}. Isso ocorre por motivo de flexibilidade na utilização
da ferramenta. O responsável por incluir as probabilidades de ocorrência pode ser qualquer \textit{stakeholder} do projeto, basta que
o mesmo tenha conhecimento sobre como o sistema deverá ser utilizado em ambiente de produção.

\begin{figure}[htb!]
\centering
\includegraphics[width=1\columnwidth]{img/processo_op_bdd}
\caption{Processo de teste de acordo com a abordagem proposta.}
\label{img:op_bdd}
\end{figure}

Além de preencher a ferramenta com as informações de probabilidades de ocorrência, o \textit{Testador/Expert} deverá informar
à ferramenta a quantidade de casos de teste desejada ao final dos ciclos de teste, assim como a quantidade de casos de
teste desejada a cada ciclo de teste. O número de ciclos necessários para conclusão do processo será baseado nesta informação,
sendo utilizado como critério de parada dos ciclos de teste. A cada ciclo a ferramenta deverá sugerir um conjunto de \textit{t}
testes a serem implementados no próximo ciclo, esta sugestão é feita a partir da análise do grau de importância de cada
entidade (\textit{Operational Profile}) ponderado com a informação do \textit{Program Spectrum}. A utilização desta última informação
se dá com o objetivo de garantir a eficiência do processo mesmo após a identificação das entidades mais críticas no sistema,
como sugere \cite{bertolino2017adaptive}. O critério de parada do processo de teste é a quantidade de casos de teste desejada pela
equipe de teste participante.



\section{Validação da Proposta}

Em relação a validação da proposta, primeiramente, o processo será aplicado a sistemas implementados na linguagem Ruby, já que
é a linguagem com maior adesão à abordagem BDD, de acordo com análise realizada no Github (TCC dos meninos). A obtenção dos objetos
de estudo de caso será feita a partir da análise de características e grau de significância na comunidade do Github.

O objetivo principal é comparar a eficiência na identificação de falhas a partir da abordagem utilizada. Para isso, será utilizado o
conceito de \textit{Mutation Test}, incluindo falhas aleatórias no sistema para que os processos de teste analisados as identifique.
A abordagem proposta deverá ser comparada com as abordagens tradicionais caixa-branca e caixa-preta, além da abordagem
utilizando puramente o conceito de \textit{Operational Profile}, como proposto por Musa \cite{musa1996operational}.

A abordagem proposta por Bertolino \cite{bertolino2017adaptive} não será utilizada como abordagem comparativa em relação a proposta nesta pesquisa.
Isto ocorre pois a abordagem proposta aqui é derivada da abordagem proposta em seu trabalho, unindo os conceitos de \textit{Operational Profile}
e \textit{Program Spectrum} em uma abordagem interativa para minimizar o esforço necessário para alcançar a confiabilidade desejada.
Entretanto, foi observada a possibilidade de adaptar a abordagem para a utilização do conceito de BDD, tornando a definição de
partições uma atividade simples e direta, dado que cada feature/cenário BDD pode ser tratada como uma partição para alocação de novos
casos de teste.

Ou seja, para validação da proposta, as seguintes atividades deverão ser realizadas:

\begin{enumerate}
	\item Identificar projetos significantes que se adequem às características desejadas

		As características desejadas incluem: Linguagem Ruby, Existência de BDD's aplicados ao \textit{back-end} e comunidade ativa.

	\item Aplicar \textit{Mutation Testing} e verificar a taxa de identificação de falhas com o conjunto existente de casos de teste.

	\item Caso existam testes caixa-branca e caixa-preta, separá-los e analisar a taxa de identificação de falhas com cada um deles separadamente.

	\item Retirar todos os casos de teste e incluir aos poucos seguindo a metodologia proposta por Musa \cite{musa1996operational}, registrando
		a taxa de identificação de falhas.

	\item Retirar os casos de teste caixa-branca, caso existam, e seguir a abordagem proposta neste trabalho, registrando a taxa de identificação
		de falhas ao longo do processo.

	\item Comparar, quantitativamente, os 4 contextos analisados.

\end{enumerate}
