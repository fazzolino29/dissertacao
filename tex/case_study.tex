\label{cap:case_study}

We evaluate our Feature-Trace approach to its ability to analyze different metrics related to the software 
testing process, and thereby enable the distribution of the test effort throughout the source code. 
This yields the following research question:

%\textit{``Can BDD artifacts be used to generate the OP and guide a prioritization testing process?"}
%\\
%\textbf{RQ: }\textit{Can BDD artifacts be used to validate and guide a prioritization process?}
\begin{mdframed}
Can BDD artifacts be used to generate the OP and guide a prioritization and selection testing process?
\end{mdframed}

Our approach proposes a method that enables the prioritization and selection of test cases from the 
extraction and analysis of metrics obtained directly or indirectly from BDD artifacts and source code. 
The metrics analyzed are (1) the \textbf{Operational Profile}, obtained from the mapping of 
BDD-related entities, (2) the \textbf{Program Spectrum} related to BDD Features and to the Unit and 
Integration test cases, and (3) the \textbf{complexity} of the method taking into account the 
Cyclomatic Complexity, the ABC Score and the number of lines (LOC) of the method.  In order to 
automate our approach, we implemented the \textit{Feature-Trace} tool, developed in Python and 
available as a GitHub project\footnote{https://github.com/BDD-OperationalProfile}.

\subsection{Systems Under Test}

   We carried out a case study in \textbf{Diaspora}: a social network highly acknowledged in the open 
   source software community. It has 12,226 \textit{stars} and 2,915 \textit{forks} in its repository 
   in \textit{Github}\footnote{https://github.com/diaspora/diaspora}. It is implemented in Ruby on Rails and 
   has a total of 72,674 lines of code, out of which 40,546 lines of Ruby code with 1,571 methods, and 
   a total of 2,975 lines of \textit{Gherkin} code, distributed throughout 68 BDD features. It also 
   has a set of 2679 unit and integration test cases.
   %O segundo software utilizado neste trabalho, ao contrário do \textit{diaspora}, é um software acadêmico que tem como objetivo facilitar e automatizar o processo de alocação de salas de aula. Este possui 14 \textit{stars} e 21 \textit{forks} em seu repositório, também no \textit{Github}\footnote{https://github.com/fga-eps-mds/2017.1-SIGS}.
    
    % A Tabela \ref{tab:comp_projects} apresenta alguns dados importantes sobre o \textit{diaspora} e o \textit{SIGS}.
    
    % \begin{table}[!htb]
    %     \caption{Dados dos projetos analisados.}
    %     \label{tab:comp_projects}
    %     \begin{tabular}{ccccc}
    %         \hline
    %         \textbf{Project} & \textbf{Lines} & \textbf{Features} & \textbf{Methods} & \textbf{Tests} \\ \hline
    %         Diaspora         & 72,674         & 68                 & 1571             & 2,679              \\
    %         SIGS             & 28,818          & 20                & x                & 202              \\ \hline
    %     \end{tabular}
    % \end{table}
    
    % A Tabela \ref{tab:comp_projects} deixa clara a relação de grandeza entre os softwares analisados neste trabalho. 
    % O \textit{Diaspora} possui um total de 72,674 linhas de código, entretanto apenas 40,546 linhas de código ruby com 1,571 métodos, e com um total de 2,975 linhas de código \textit{Gherkin} distribuídas em 68 features, utilizado para registrar as \textit{Features} e \textit{Scenarios} BDD. O \textit{SIGS} possui um total de 28,818 linhas de código, com 5,444 linhas de código ruby com \textbf{NUMERO MÉTODOS} e 601 linhas de código \textit{Gherkin} distribuídas em 20 features.
    
    % A abordagem proposta neste trabalho foi aplicada nos dois softwares apresentados e os dados obtidos, assim como as analises realizadas são apresentadas a seguir.

\subsection{Experimental Setup}

To analyze the Feature-Trace approach some tools and configurations have been adopted. In order to 
obtain the execution trace of the scenarios and unit/integration tests, we use the 
\textit{Simplecov}\footnote{https://github.com/colszowka/simplecov} tool, making it possible to 
extract the trace of each software execution in the JSON format, facilitating the reading by the 
Feature-Trace tool for traces analysis. Note that the \textit{Simplecov} tool provides a JSON file 
containing the file names and line numbers with their occurrence information. 
%In this case study, some directories are ignored by running trace analysis, avoiding the extraction 
%of methods that do not match the executed feature. 
%The skipped directories are: \textit{migrations}, \textit{db}, \textit{.git}, \textit{log}, \textit{public}, \textit{script}, \textit{spec}, \textit{tmp}, \textit{vendor}, \textit{docker}, \textit{coverage}, \textit{config}, \textit{bin} e \textit{features}.

The initial process of the analysis occurs with the static analysis of the SUT source code. This 
analysis is done in an automated way by the Feature-Trace tool. Some information is obtained from 
the use of the \textit{Excellent}\footnote{https://github.com/simplabs/excellent} tool, which makes 
it possible to obtain the Cyclomatic Complexity, ABC Score and Number of lines of the method. Such 
automation has also been integrated into the Feature-Trace tool.  The execution of the BDD scenarios 
is done with the support of the \textit{Cucumber}\footnote{https://cucumber.io/} tool, being executed 
for each scenario present and also integrated into our Feature-Trace tool. 

\subsection{Results and Analysis}
    The application of the Feature-Trace approach in the Diaspora project made it possible to analyze 
    the ability of Feature-Trace to extract relevant information from software projects using the 
    BDD approach. 
    
    
% A Figura \ref{img:tests_complexity} apresenta a relação de crescimento do número de casos de 
%teste em relação a ao crescimento da complexidade do método.
    
%     \begin{figure}[!htb]
%       \includegraphics[width=1\linewidth]{crescimento_tests.png}
%     	\centering
%       \caption{Growth rate of the number of test cases.}
%       \label{img:tests_complexity}
%     \end{figure}
    
    The extraction of information made it possible to identify the distribution of the testing effort 
    taking into account several questions, as presented in the Table~\ref{tab:methods_information}. 
    We should note that, for the sake of space, it is not possible to list all methods of Diaspora in 
    this work. We provide the full list of Diaspora software methods analysis, as well as all the 
    artifacts involved in this case study, at Github\footnote{https://github.com/FeatureTrace/SBES2019}.
    
    Table\ref{tab:methods_information} shows that we obtain the OP, the number of IF (impacted features) 
    and the number of test cases that exercise each SUT method. We should note that, although 
    Feature-Trace also gathers static information regarding the software complexity, such as Cyclomatic 
    Complexity, ABC Score and Number of Lines, this information has been removed from 
    Table~\ref{tab:methods_information} to highlight those information at stake as an outcome of our 
    approach. Also, we selected one method from each importance group (based on the OP) as an example.

Based on the outcome of Feature-Trace on Table~\ref{tab:methods_information} for the Diaspora software, 
we should prioritize more test cases to method M1 (which has the highest OP), or we should focus on 
method M2 which contains few test cases and a considerably higher number of IF compared to method M3? 
%This type of discussion is essential to the success of the test case prioritization process. In this way, the team should use this information to weight and distribute the testing effort throughout the source code.

In addition to the information presented in Table~\ref{tab:methods_information}, the presented methods 
M1, M2 and M3 also have information regarding the testing effort of these methods based on cyclomatic 
complexity, ABC score, and LOC. That is, the testing team can analyze the situation and decide which 
unit of code should be tested in the next testing cycle, for example, the unit that has the highest 
OP and lowest Spectrum? But what if we take into account the effort of the team in conducting this 
test? Is this order maintained? Discussions like this are very important to the software testing 
process, highlighting one of the benefits promoted by our Feature-Trace approach.
    
\small
    \begin{table}[!htb]
    \centering
    \caption{Methods information obtained with Feature-Trace.}
    \label{tab:methods_information}
    \begin{tabular}{lccc}
\hline
\multicolumn{1}{c}{\textbf{Method}}                                                                  & \textbf{Occ. Prob.} & \textbf{Imp. Feat.} & \textbf{Spec.} \\ \hline
\textit{\begin{tabular}[c]{@{}l@{}} person\_image\_link\\ PeopleHelper (M1)\end{tabular}}               & \textit{88.4785\%}  & 61                  & 2679           \\\hline
\textit{\begin{tabular}[c]{@{}l@{}} current\_user\_person\_contact\\ PersonPresenter (M2)\end{tabular}} & \textit{53.8023\%}  & 45                  & 92             \\\hline
\textit{\begin{tabular}[c]{@{}l@{}}3. extension\_whitelist\\ UnprocessedImage (M3)\end{tabular}}          & \textit{16.3767\%}  & \textit{14}         & 116            \\ \hline
\end{tabular}
\end{table}
\normalsize
    
To analyze the generation of the OP presented in Section \ref{sec:analyser}, we take into account two 
user profiles of Diaspora software, which we call UP1 (User Profile 1) and UP2 (User Profile 2). The 
probability of occurrence of the features and scenarios of both hypothetical profiles were generated 
taking into account a profile of a typical user of the social network, which uses it as an 
entertainment medium (UP1), and a profile of a user that uses the social network as a Digital 
Influencer (UP2). Table \ref{tab:OP_validation} presents some information about the OP generated for 
the Diaspora software. Among them, there are the features and scenarios most and least executed by 
each User Profile, as well as the most executed method by both UP1 and UP2. It can be noticed that 
for both user profiles, the most executed method is \textit{``person\_image\_link"}. It is due to 
the fact that the Diaspora software is a social network that needs to render the user's photo in 
several scenarios of usage. For better visualization, we choose among those methods that do not 
occur during the execution of all the features and scenarios of the SUT. We postulate that the 
methods common to all features and scenarios (107 methods) are related to the pre-configuration 
required by all features executions. This set of methods should be analyzed in the next works.

    \begin{table}[!htb]
    \centering
    \caption{Example of the OP distribution.}
    \label{tab:OP_validation}
    \begin{tabular}{ccc}
    \hline
    \textbf{Users Profiles}                                                     & UP1                                                                                               & UP2                                                                                                        \\ \hline
    \textbf{\begin{tabular}[c]{@{}c@{}}Feature with \\ Highest OP\end{tabular}} & \textit{\begin{tabular}[c]{@{}c@{}}Preview Posts in \\ the Stream\\ (2.9372\%)\end{tabular}}      & \begin{tabular}[c]{@{}c@{}}The public stream\\ (2.6668\%)\end{tabular}                                     \\
    \textbf{\begin{tabular}[c]{@{}c@{}}Feature with\\ Lowest OP\end{tabular}}   & \textit{\begin{tabular}[c]{@{}c@{}}Auto follow\\ back a user\\ (0.0559\%)\end{tabular}}           & \begin{tabular}[c]{@{}c@{}}Managing authorized \\ applications\\ (0.0004\%)\end{tabular}                   \\
    \textbf{\begin{tabular}[c]{@{}c@{}}Scenario with\\ Highest OP\end{tabular}} & \textit{\begin{tabular}[c]{@{}c@{}}preview a post \\ on tag page\\ (0.6590\%)\end{tabular}}       & \textit{\begin{tabular}[c]{@{}c@{}}seeing public \\ posts as a logged \\ out user\\ (1.5330)\end{tabular}} \\
    \textbf{\begin{tabular}[c]{@{}c@{}}Scenario with \\ Lowest OP\end{tabular}} & \textit{\begin{tabular}[c]{@{}c@{}}preview a photo \\ with text\\ (0.1871\%)\end{tabular}}        & \textit{\begin{tabular}[c]{@{}c@{}}revoke an \\ authorization\\ (0.0002\%)\end{tabular}}                   \\
    \textbf{\begin{tabular}[c]{@{}c@{}}Top OP \\ Method\end{tabular}}           & \textit{\begin{tabular}[c]{@{}c@{}}person\_image\_link\\ PeopleHelper\\ (83.7784\%)\end{tabular}} & \textit{\begin{tabular}[c]{@{}c@{}}person\_image\_link\\ PeopleHelper\\ (84.6296\%)\end{tabular}}          \\ \hline
    \end{tabular}
    \end{table}
    
    % \begin{figure}[!htb]
    %   \includegraphics[width=1\linewidth]{tests_by_folder.png}
    % 	\centering
    %   \caption{Number of test cases by code folder \textbf{rever}.}
    %   \label{img:tests_by_folder}
    % \end{figure}
    
    % A extração do esforço de teste possibilitou, também, algumas análises relacionadas a taxa de crescimento dos casos de teste unitários/integração baseado no número de Features impactadas, como pode ser observado na Figura \ref{img:tests_by_IF}. Esta informação é importante quando se deseja garantir que o conjunto de métodos utilizados como base para o  funcionamento do maior número de features está sendo verificado o suficiente, já que uma falha em um método base pode inviabilizar o funcionamento do software como um todo. 

    % \begin{figure}[!htb]
    %   \includegraphics[width=1\linewidth]{tests_by_IF.png}
    % 	\centering
    %   \caption{Number of test cases by Impacted Features \textbf{rever}.}
    %   \label{img:tests_by_IF}
    % \end{figure}
    
    Regarding the global view of the SUT,  Figure~\ref{img:grafo_entities} shows the representation of 
    the \textit{``Preview Posts in the Stream"} feature (in orange), as the main feature of UP1, its 
    scenarios (in purple), and corresponding invoked methods (in green). In a top-down view, the image 
    allows a broad view of the set of methods that are involved with all scenarios (automatically 
    positioned at the center of the graph). Therefore, these are the essential methods for running 
    the Feature as a whole. It is also important to highlight the specific methods of each feature 
    usage scenario (prominently positioned). This graph-view perspective represents the mapping of 
    entities of the Diaspora feature \textit{``Preview Posts in the Stream"} and makes evident the 
    software traceability, allowing the knowledge of the software product as a whole into various 
    abstraction levels.
    
    \begin{figure}[H]
      \includegraphics[width=0.9\linewidth]{feature_diaspora.png}
    	\centering
      \caption{Global view of the feature \textit{``preview posts in the stream"}.}
      \label{img:grafo_entities}
    \end{figure}

    In addition to the top-down visualization, the Visualizer Module made possible the representation 
    of a bottom-up traceability of the requirements from the SUT method. In the center of 
    Figure~\ref{img:method_traceability} lies the representation of the method 
    \textit{``person\_image\_link"} presented earlier in Table~\ref{tab:OP_validation}. From this 
    visualization it is possible to identify the requirements impacted by the 
    \textit{``person\_image\_link"} method. This information allows the testing team to know the 
    magnitude of the impact of a particular change in the method's source code.
    
    %A partir desta informação e do número de casos de teste que exercitam este método, apresentado na Tabela \ref{tab:methods_information}, a equipe de teste pode ponderar o processo de priorização de casos de teste, de acordo com a estratégia desejada.
    
    \begin{figure}[H]
      \includegraphics[width=0.8\linewidth]{method_map.png}
    	\centering
      \caption{Traceability perspective of method \emph{``person\_image\_link"}.}
      \label{img:method_traceability}
    \end{figure}

%Este conjunto de visualizações e informações das entidades do software viabiliza a aplicação de diversas técnicas de priorização de casos de teste, desde técnicas caixa-preta até técnicas caixa-branca. Os trabalhos de \cite{henard2016comparing} e \cite{miranda2018fast} discutem algumas das técnicas comuns de priorização de casos de teste. Observou-se que os dados obtidos a partir da nossa abordagem, Feature-Trace,  possibilitam a aplicação de diversas das técnicas citadas. Entre as técnicas de priorização caixa-branca, a abordagem Feature-Trace suporta tanto as técnicas baseadas em \textit{Total Coverage}, onde são priorizados os casos de teste que cobrem o maior número de unidades do código fonte, quanto as técnicas baseadas em \textit{Additional Coverage}, as quais buscam selecionar os casos de teste que cobrem o maior número de entidades ainda não cobertas pelo conjunto de casos de teste já executados. A abordagem possibilita, ainda, a utilização de técnicas de priorização que se baseiam na diversidade do trace gerado, utilizando, por exemplo, Global Maximum Distances algorithms~\cite{henard2014bypassing}. Em relação à técnicas caixa-preta de priorização de casos de teste, a abordagem Feature-Trace possibilita a utilização de técnicas baseadas nos requisitos~\cite{arafeen2013test} e utilizando conceitos como o OP~\cite{musa1993operational}.

% \del{\chg{Using as an example the Table~\ref{tab:methods_information}, we}{Based on the outcome of Feature-Trace on Table~\ref{tab:methods_information} for the Diaspora software, one can learn that more tests} should \chg{include more tests in}{be performed for} method \chg{1}{M1} (which has the highest OP), or \ins{that} we should focus on methods \chg{2}{M2} \del{and \chg{3}{M3}} \chg{ that, despite having low OP, contains}{, which contain} few test cases and a considerable number of IF\chg{?}{compared to method M3.}}

% \del{In addition to the information presented in Table~\ref{tab:methods_information}, these methods also have information regarding the test effort of these methods.(Cyclomatic Complexity, ABC Score, and Number of Lines). That is, the team can analyze the situation and decide which unit of code should be tested in the next test cycle: the unit that has the highest OP and lowest Spectrum? But what if we take into account the effort of the team in conducting this test? Is this order maintained? Discussions like this are very important to the software testing process, highlighting one of the advantages of using the proposed approach.}


\subsection{Threats to Validity}
    % \gena[Muito importante essa seção! Validade de construção do experimento, validade interna do experimento, validade externa do experimento, e confiabilidade (replicabilidade dos experimentos)]
    
    The results presented are subject to some threats to the validity of the case study and should be 
    highlighted. 
    
    Related to the \emph{construct validity}, the generation of OP is based on the hypothetical usage 
    profiles, which make unrealistic the comparative analysis between the degree of importance of the 
    source code unit and the number of test cases exercised. However, since the objective of the study 
    is to present the capability to support the prioritization of test cases of the SUT, these 
    hypothetical data do not affect the validity of the obtained results and the performed analysis. 
    If the OP has real data, the analysis becomes realistic and the prioritization process can rely on 
    the actual data of the involved OPs. 
    
     Regarding the \emph{internal validity}, we highlight the fact that we used only some of the 
     metrics useful for the process of prioritizing test cases. However, we sought to select at least 
     one metric related to the impact on reliability (OP), three metrics related to the testing effort 
     (complexity) and one metric referring to the current coverage of each computational unit. 
    
    Concerning \textit{external validity}, we considered as a threat the case study is focused only on 
    Diaspora. Further case studies must be performed so that the ability to generalize our results can 
    be validated. However, we should note that Diaspora is a software used by real software engineering 
    development teams and has been focus of significant number of contributions in the open source 
    software community.
    
    Finally, to make feasible the replication of our outcomes in the case study, all the artifacts of 
    the evaluation process are publicly available on 
    Github\footnote{https://github.com/FeatureTrace/SBES2019}.
    
    % Em relação a \textit{external validity}, destacamos, principalmente, a utilização de apenas um projeto escrito em Ruby on Rails e utilizando a ferramenta Cucumber para aplicação da abordagem BDD. O quesito de generalização da abordagem se mostra impactado devido a outras tecnologias não serem analisadas, como Java, JavaScript, Python e etc. Além disso, análises em sistemas menores e mais simples podem ser de grande valia em relação a generalização do estudo, dado que o projeto analisado é referente a uma rede social grande e complexa. Por fim, a replicabilidade do estudo de caso se mostra adequada, dado que todos os artefatos utilizados se encontram disponíveis para consulta e análises realizadas por terceiros.
    
    

    % Explicar como foi a coleta dos dados.
    % Utilizar a execução dos BDD's para priorizar casos de teste unitários/integração. Incluir mutantes e avaliar qual a importância de cada mutante.. a partir da priorização dos casos de teste, vai registrando a confiabilidade. Compara com uma priorização padrão e verificar quem encontra o maior número de mutantes importantes.
    
    % Comprovar o funcionamento da distribuição de probabilidades entre features/cenários/métodos. Fazer o teste com o método de geração aleatória de probabilidades.
    
    % Priorizar casos de teste spec baseados no OP e no Spectrum dos BDDs (quantas features) são afetadas por cada método). Fazer a priorização com mutantes no Reserva de Salas e no Diaspora (parte dele).
    
    % Como foi detalhado na seção anterior, o mapeamento entre entidades ocorre de maneira estática e dinâmica, onde a análise estática possibilita a obtenção das entidades que envolvem a visão do usuário do sistema, ou seja, deste o mais alto nível até o nível de \textit{key input variable}. 
    % Já a análise dinâmica ocorre a partir da extração de informações do \textit{trace} gerado pela ferramenta \textit{Simplecov}, identificando quais entidades do código compõem determinada funcionalidade ou cenário de uso. Com o mapeamento de entidades concluído, após a obtenção das informações de probabilidades das entidades de alto nível, é feita a distribuição das probabilidades para o nível de OP. Com o objetivo de validar a distribuição das probabilidades, buscamos comparar a distribuição feita de maneira manual com a distribuição automatizada. Para isso, os dois métodos de distribuição se basearam em \textit{inputs} aleatórios das probabilidades das entidades de alto nível de tal forma que um \textit{input} aleatório teve suas probabilidades distribuídas tanto de maneira automática quanto de maneira manual, realizando uma comparação entre os resultados obtidos para verificar que a distribuição ocorre da maneira esperada.
    
    % Para isso, utilizamos um produto de software chamado \textit{Reserva de Salas}\footnote{https://github.com/EngSwCIC/Reserva-de-Salas}, destacando apenas duas \textit{Features} e seus respectivos \textit{scenarios} e métodos, que podem ser observados na Figura \ref{img:valid_prob_distrib}. Nesta representação, \textbf{Pf} se refere a \textit{probabilidade da feature}, \textbf{Ps} se refere a probabilidade dos \textit{scenarios} (ou \textit{key-input variable}) e, da mesma forma, \textbf{Pm} se refere a probabilidade das entidades do código (métodos) que compões as \textit{features} analisadas.

    
    
% \begin{table*}[!htb]
% \caption{Validação da distribuição das probabilidades via ferramenta Feature-Trace.}
% \label{tab:methods_analise}
% \begin{tabular}{llll}
% \hline
% \multicolumn{1}{c}{\textbf{\begin{tabular}[c]{@{}c@{}}Functional \\ Profile\end{tabular}}} & \multicolumn{1}{c}{\textbf{\begin{tabular}[c]{@{}c@{}}Key-input \\ Variable\end{tabular}}}                  & \multicolumn{1}{c}{\textbf{\begin{tabular}[c]{@{}c@{}}Automatic\\ distribution\end{tabular}}}                                                                                                                              & \multicolumn{1}{c}{\textbf{\begin{tabular}[c]{@{}c@{}}Manual\\ distribution\end{tabular}}}                                                                                                                                 \\ \hline
% \begin{tabular}[c]{@{}l@{}}Pf1 = 84.8484\%\\ Pf2 = 15.1515\%\end{tabular}                  & \begin{tabular}[c]{@{}l@{}}Ps1 = 29.7713\%\\ Ps2 = 55.0770\%\\ Ps3 = 6.5151\%\\ Ps4 = 8.6363\%\end{tabular} & \begin{tabular}[c]{@{}l@{}}Pm1 = 84.8484\%\\ Pm2 = 84.8484\%\\ Pm3 = 84.8484\%\\ Pm4 = 99.9999\%\\ Pm5 = 99.9999\%\\ Pm6 = 99.9999\%\\ Pm7 = 15.1515\%\\ Pm8 = 15.1515\%\\ Pm9 = 15.1515\%\\ Pm10 = 15.1515\%\end{tabular} & \begin{tabular}[c]{@{}l@{}}Pm1 = 84.8483\%\\ Pm2 = 84.8483\%\\ Pm3 = 84.8483\%\\ Pm4 = 99.9997\%\\ Pm5 = 99.9997\%\\ Pm6 = 99.9997\%\\ Pm7 = 15.1514\%\\ Pm8 = 15.1514\%\\ Pm9 = 15.1514\%\\ Pm10 = 15.1514\%\end{tabular} \\
%                                                                                           &                                                                                                             &                                                                                                                                                                                                                            &                                                                                                                                                                                                                           
% \end{tabular}
% \end{table*}

    % \begin{table}[!htb]
    %     \caption{Random probability values.}
    %     \label{tab:valid_prob_distrib}
    %     \begin{tabular}{cc}
    %         \hline
    %         \textbf{Feature}                                & \textbf{Scenario}                        \\ \hline
    %         \multirow{2}{*}{\textit{Book room (10.2803\%)}} & \textit{book a room - Success (5.6909\%)} \\
    %                                                         & \textit{book a room - Fail (4.5894\%)}    \\ \hline
    %         \textit{Edit User (9.6573\%)}                   & \textit{Edit user (9.6573\%)}            \\ \hline
    %     \end{tabular}
    % \end{table}
    
    % Sabe-se que a probabilidade total das \textit{Features} deve se igualar a probabilidade total do software, se considerarmos apenas um \textit{customer profile}, \textit{System-mode profile} e \textit{User profile}.  A Tabela \ref{tab:methods_analise} apresenta as \textit{features}, seus respectivos \textit{scenarios} (ou \textit{key input variables}) e as probabilidades dos métodos, obtidas de maneira automática, utilizando tanto a ferramenta Feature-Trace quanto um processo manual, com o objetivo de avaliar a corretude da ferramenta proposta em relação à distribuição automática das probabilidades de ocorrência. Com base nesses dados, observamos que a ferramenta proposta é capaz de distribuir as probabilidades de forma precisa ( [e rápida?] \textbf{Pensar se é necessário incluir análise de performance aqui}). A pequena variação no último dígito dos resultados ocorre devido ao número de casas decimais utilizadas na validação manual, sendo menor que o número total de casas decimais presentes em variáveis \textit{python} do tipo \textit{float}. O processo manual utilizou quatro casas decimais para distribuição das probabilidades.
    
    % De acordo com a Tabela \ref{tab:valid_prob_distrib}, o algoritmo de distribuição das probabilidades entre \textit{scenarios} e métodos se comporta da maneira esperada. Assim sendo, podemos utilizá-lo para obter, diretamente, o OP do software analisado a partir da informação de probabailidade de cada \textit{feature} e seus \textit{scenarios}.
    
%     \begin{table*}[!htb]
%     \caption{Exercised methods for each Scenario.}
%         \label{tab:methods_analise}
% \begin{tabular}{cc}
% \hline
% \textbf{Method}                                                                                           & \textbf{Scenario}                \\ \hline
% \multirow{3}{*}{\textit{\begin{tabular}[c]{@{}c@{}}is\_admin?\\ after\_sign\_in\_path\_for\end{tabular}}} & Book a Room - Success (5.6909\%) \\
%                                                                                                           & Book a Room - Fail (4.5894\%)    \\
%                                                                                                           & Edit User (9.6573\%)             \\ \hline
% \multirow{2}{*}{\textit{\begin{tabular}[c]{@{}c@{}}create\\ show\\ index\\ signed\_in?\end{tabular}}}     & Book a Room - Success (5.6909\%) \\
%                                                                                                           & Book a Room - Fail (4.5894\%)    \\ \hline
% \textit{\begin{tabular}[c]{@{}c@{}}edit\\ update\\ configure\_account\_update\_params\end{tabular}}       & Edit User (9.6573\%)             \\ \hline
% \end{tabular}
% \end{table*}
    