The software development process involves several challenges that need to be overcome by the development 
team for the product to be delivered successfully. The challenges start from the first stage of the 
process, in the requirements elicitation phase, to the last stage, in the software maintenance 
and evolution phase. The causes of the problems can vary widely, but according to~\cite{delamaro2017introduccao}, 
most of them are the result of human errors during the development process. Thus, the software lifecycle 
is subject to the inclusion of failures at all times, even in experienced and skilled teams.

In this sense, the activities of Verification and Validation
identify failures so that they are corrected before the delivery of the system. According to~\cite{adrion1982validation},
Verification and Validation activities must occur during the entire system development cycle, seeking to identify
failures as soon as possible, saving time and money. These activities are usually automated from the implementation of test 
cases, making it possible to generate regression test suites that can check the software whenever a new change is made~\cite{myers2011art}.

Test cases are usually classified in white- and black-box testing, where the first is more related to the activity of Software 
Verification, also known as structural testing. On the other hand, the black-box tests seek to validate the software product, 
that is, check if the software meets the requirements elicited~\cite{adrion1982validation}.

Given the notorious challenge regarding the verification and validation of software completeness through test 
cases~\cite{catal2013test}, it is paramount to delimit a relevant 
set of test cases that guarantees the highest possible reliability within the limitations 
of the software development team~\cite{myers2011art}. Aiming at this goal, several techniques 
for prioritizing test cases have been proposed, related to the prioritization of either 
white- or black-box testing or both~\cite{henard2016comparing}. The prioritization process 
of test cases can occur from several strategies. These strategies use the data from the 
system under test (SUT) to define the prioritization criteria of test cases, such as the 
complexity of code units (effort to test that unit)~\cite{ebert2016cyclomatic}, Operational 
Profile (OP) information~\cite{musa1993operational}, history of failures~\cite{noor2015similarity} 
and similarity of the test cases~\cite{miranda2018fast}, among others~\cite{catal2013test}. 
Overall, these strategies focus mostly on software verification and validation activities, 
which seek to ensure that the software is being developed in the right way and that the 
correct software is being developed, respectively~\cite{myers2011art}.

In particular, many techniques are based on concepts such as OP, which takes into account the probability of execution of each 
entity of the software to provide quantitative information about how the software will be used~\cite{musa1996operational}. 
There are several advantages of using the OP in the software development cycle: (1) to reduce costs with operations with little 
impact on system reliability; (2) to maximize the speed of development, from the proper allocation of resources, 
 taking into account the usage and the criticality of the operations; (3) to guide effort distribution in requirements, code, 
 and design revisions, and (4) to make testing more efficient (greater slope in system reliability growth rate). 
 However, there are several limitations that often render impractical the use of the OP in the software development cycle. 
 The effort and the complexity involved in generating the OP are considerably high as well as the tendency to the stability of 
 the reliability growth during the process of prioritizing test cases. That is, after a certain number of test cycles, the growth 
 rate of reliability tends to be stable due to the non-prioritization of units that have a low probability of 
 occurrence.~\cite{bertolino2017adaptive}.

% Thus, the construction of the OP from the BDD artifacts is mainly focused on the mapping between OP entities and BDD entities.

It is worth noting that the quality of software products involves not only the quality of the source code but also the quality 
of other software artifacts, e.g. requirements specification and tests suites, and the traceability between such 
artifacts~\cite{rempel2017preventing}. 
Moreover, the success of software testing activity is directly connected to the correct alignment between requirements and test 
cases~\cite{bjarnason2014challenges}. The lack of traceability between artifacts can lead a software project to complete 
failure~\cite{rempel2017preventing}. Communication between requirements engineers and test engineers is often compromised by 
dealing with abstraction levels, languages and entirely unconnected complexities~\cite{rempel2017preventing}. Thus, several 
studies have proposed strategies and techniques for locating requirements in the source code~\cite{eisenbarth2003locating, liu2007feature, eaddy2008cerberus, beck2015rethinking, li2017fhistorian}.

%The study presented by~\cite{rempel2017preventing} shows that the degree of traceability between software artifacts have a statistically significant impact on the number of defects and therefore the quality of the final product. That is, the lack of traceability between artifacts can lead a software project to complete failure. Communication between requirements engineers and test engineers is often compromised by dealing with abstraction levels, languages ​​, and entirely unconnected complexities. Thus, 
Some approaches have emerged to bring the requirements artifacts closer together and Verification and Validation. In this sense, 
it is worth highlighting the methodologies of agile development, which have sought to minimize the gap between the Requirements 
activities and the Verification and Validation software activities~\cite{inayat2015systematic}. Some practices of the agile 
development process deserve to be highlighted when we refer to the gap between the activities of Requirements Elicitation and 
Verification and Validation, such as:
\begin{itemize}
  \item \textit{Face-to-face communication} %between the development team and project stakeholders, 
  % minimizing bureaucracy and formality in communication.
  \item \textit{Customer involvement and interaction} 
  \item \textit{User stories}
  \item \textit{Change management}
  \item \textit{Review meetings and acceptance tests}
\end{itemize}

Among the approaches used to minimize this gap and which are gaining notoriety is the \textit{Behavior Driven Development} 
(BDD)~\cite{BDD_dan_north}. In BDD, system requirements and test cases merge into the same artifact, making the requirement 
automatically verifiable and enabling a familiar interface among all stakeholders in the software project. 
The approach uses a ubiquitous language for writing artifacts in the form of user stories. However, the adoption of the BDD 
approach also involves some challenges, such as (i) poorly written scenarios, i.e. scenarios lacking comprehensive requirements representation, 
which may render the test assurance process weak and (ii) difficulty in convincing the stakeholders to use BDD, since it requires 
a paradigm shift from the stakeholders to specify the features in BDD language~\cite{pereira2018behavior}. Moreover, BDD artifacts 
do not provide quantitative information to support test case prioritization, unlike OP.

% Como uma evolução do conceito de Perfil Operacional, este trabalho utiliza a abordagem proposta por \cite{bertolino2017adaptive}, chamada de \textit{Covrel}, a qual utiliza informações caixa-preta (a partir do Perfil Operacional) em conjunto com informações de caixa-branca, dadas pelo \textit{program spectrum}.

Given the gaps and difficulties presented to ensure the traceability between requirements and software testing, as well as the 
lack of an approach that provides a solid way of applying the concepts inherent to the OP, we propose the \textit{Feature-Trace} 
approach. 
% Feature-Trace promotes the understanding of the software product from the perspective of the tested requirements as BDD artifacts, and their relevance enabling the traceability and testability of the requirements through the construction of the OP and its consequent definition of importance of each software entity. Such importance is able to achieved through the number of impacted entities from the requirements to the units of the code and the available unit and integration test cases. Thus, from the BDD artifacts and the source code of the system under test (SUT), relevant information is extracted to guide the software testing process.
Feature-Trace facilitates the understanding of the software product from the perspective of the tested requirements as BDD 
artifacts and their attributed relevance. Then, leveraged by the construction of the OP from the BDD artifacts, Feature-Trace 
enables the traceability, testability and quantitative measure of importance from the requirements to the code units and their 
corresponding test cases. By these means, from the BDD artifacts and the SUT source code, relevant information is seamlessly 
extracted to guide the software testing process.
%\del{Entre as informações extraídas a partir da abordagem proposta estão: \textit{Operational Profile}, \textit{Program Spectrum}, \textit{Impacted Features} e a complexidade de cada unidade do código fonte.}

The proposed approach was evaluated on the Diaspora social network software, an open source project, which contains 68 BDD features, 
specified in 267 scenarios. Diaspora has approximately 72 thousand lines, with 2,915 \textit{forks} and 12,226 \textit{stars} in 
its repository in Github\footnote{https://github.com/diaspora/diaspora}. The case study revealed that the Feature-Trace 
approach is capable of extracting the OP as well as obtaining and presenting vital information to guide the prioritization 
process of test cases.

In a nutshell, the contributions of this work are threefold:
\begin{enumerate} 
\item An approach that allows for: (1) the creation of OP seamlessly stemmed from well-formed, executable and traceable 
requirements and (2) the prioritization of software testing based on the created OP as well as on a set of quantitative SUT 
information, i.e, program spectrum, number of impacted software features and code complexity.
\item A tool that automates the proposed approach, allowing the application of the approach without great effort by the testing 
team primarily based on the BDD scenarios specifications, source code implementation and test suite.
\item A case study on the Diaspora social network as a GitHub open project that shows the applicability and the potential of 
our approach.
\end{enumerate}

The remaining sections are organized as follows: Chapter \ref{cap:backgroung} presents key background concepts. 
Chapter \ref{cap:approach} provides an in-depth perspective of our Feature-Trace approach. Chapter \ref{cap:case_study} 
presents the case study carried out on the Diaspora software. Chapter \ref{cap:related_work} compares our approach to the 
most related work. Finally Chapter \ref{cap:conclusion} concludes the work with some discussions, lessons learned and outlines 
future works.



% ------------------------- PORTUGUES ------------------------

% O processo de desenvolvimento de software envolve diversos desafios que precisam ser superados pela equipe de
% desenvolvimento para que o produto seja entregue com sucesso. Os desafios têm início desde a primeira etapa do processo,
% na fase de elicitação dos requisitos, até a última etapa, na fase de manutenção e evolução do software. As causas dos problemas
% podem variar bastante, mas de acordo com \cite{delamaro2017introduccao}, a maioria deles é fruto de erros humanos durante o
% processo de desenvolvimento. Assim, o ciclo de vida do software está submetido a inclusão de falhas a todo momento, mesmo em
% equipes experientes e habilidosas.

% Buscando minimizar a quantidade de erros inclusos durante o processo de desenvolvimento, diversas técnicas e estratégias vêm sendo
% propostas pela comunidade, dando luz ao que hoje conhecemos por \textit{Engenharia de Software}. A disciplina de Engenharia de Software
% engloba métodos e técnicas que facilitam o processo de desenvolvimento de software, minimizando a probabilidade de inclusão de
% falhas por erros humanos. Segundo \cite{sommerville2007software},
% a importância da Engenharia de Software se dá por: 1) cada ves mais a sociedade depende de produtos de software para sobreviver e evoluir,
% exigindo o desenvolvimento de sistemas confiáveis de maneira econômica e rápida; e
% 2) a longo prazo, o desenvolvimento de software se torna mais barato com a utilização de técnicas da Engenharia de Software,
% já que modificações no software após a entrega do mesmo, com manutenção, exigem bastante esforço da equipe de desenvolvimento, se comparado
% com modificações realizadas no começo do projeto.

% Os erros podem ocorrer a qualquer momento do processo de desenvolvimento, desde as primeiras reuniões de elicitação dos requisitos do sistema
% até o período de implantação do sistema, incluindo, ainda, a manutenção e evolução do software. Desse modo, com o objetivo de
% garantir que o produto entregue, após todo o período de desenvolvimento (e inclusão de falhas), é o produto correto e funciona da maneira adequada,
% surge o conceito de Verificação e Validação de software \cite{delamaro2017introduccao}. O processo de Verificação e Validação
% busca identificar falhas para que as mesmas sejam corrigidas antes da entrega do sistema. De acordo com \cite{adrion1982validation},
% as atividades de Verificação e Validação devem ocorrer durante todo o ciclo de desenvolvimento do sistema, buscando identificar
% falhas o mais cedo possível, economizando tempo e dinheiro.

% Para facilitar o processo de Verificação e Validação, a comunidade buscou automatizar a execução de testes de software, que aplicam
% entradas e verificam se a saída esperada é igual a saída obtida, garantindo que a funcionalidade (ou função) testada está sendo executado da maneira esperada.
% Com esta atividade de teste, a equipe de desenvolvimento busca evitar a inclusão de novas falhas durante o desenvolvimento \cite{sommerville2007software}.
% De acordo com \cite{delamaro2017introduccao} os casos de teste podem ser aplicados com diferentes objetivos e em diferentes níveis, podendo ser
% classificados em:

% \begin{itemize}
%   \item Teste Caixa-preta

%   Neste tipo de teste, o software é visto como uma caixa preta, utilizando os casos de teste para incluir \textit{inputs} e observar
%   \textit{outputs} sem se preocupar com a execução interna do sistema. Neste tipo de teste, o objetivo é garantir que as funcionalidades
%   estão funcionando da maneira adequada, de acordo com o estabelecido nos requisitos. Uma vantagem da utilização do teste caixa-preta
%   é a independência do código do sistema, já que a maneira com que o sistema foi implementado não importa, desde que cada funcionalidade
%   retorne o \textit{output} esperado de acordo com o \textit{input} submetido. Entretanto, o teste caixa-preta possui a desvantagem
%   da possibilidade de determinadas áreas críticas do software nunca serem executadas apenas com os testes caixa-preta, já que, dependendo
%   dos \textit{inputs}, o fluxo executado pode mudar bruscamente.

%   \item Teste Caixa-branca

%   Ao contrário do teste caixa-preta, o alvo do teste caixa-branca é a execução interna do sistema, buscando garantir que cada
%   método do sistema esteja executando da maneira adequada. Os casos de teste executam métodos específicos do sistema, sem executar o
%   fluxo completo de cada funcionalidade. Uma vantagem da utilização do teste caixa-branca é a garantia de que áreas específicas
%   do sistema sejam executadas, garantindo o funcionamento de cada unidade do sistema. Algumas desvantagens de sua utilização
%   fazem referência a visão global do sistema e a quantidade de unidades em seus distintos contextos (\textit{inputs}).
%   Com o foco voltado em cada unidade em contextos isolados, alguns casos de teste podem ser implementados em unidades que nunca
%    serão executadas, enquanto algumas unidades/contextos bastante executados acabam não sendo testados devido ao prazo e verba do projeto.
% \end{itemize}

% Desse modo, vê-se que as duas abordagens de teste possuem suas vantagens e desvantagens, as quais se mostram bastante complementares. Diversos
% autores sugerem que as abordagens de teste caixa-branca e caixa-preta devem ser aplicadas de maneira complementar, obtendo as vantagens
% das duas abordagens \cite{sommerville2007software}, \cite{delamaro2017introduccao}, \cite{gregory2014more} e \cite{jorgensen2013software}.

% Os casos de teste (principalmente caixa-preta) são criados baseados nos requisitos elicitados com o cliente. Entretanto, geralmente,
% as atividades de elicitação e definição dos casos de teste são realizadas separadamente, abrindo brechas para novas inclusões de falhas,
% como afirma \cite{bjarnason2014challenges} em seu trabalho sobre os desafios do alinhamento entre os requisitos e a atividade de
% Verificação e Validação de software. Desse modo, algumas abordagens surgem buscando a aproximação dos artefatos de requisitos e dos
% artefatos de verificação e validação.

% Metodologias de desenvolvimento ágil surgem como possível solução de diversos problemas relacionados a distância entre o cliente e a equipe
% de desenvolvimento e, consequentemente, entre os requisitos e o código do sistema. Estas envolvem um conjunto de práticas
% utilizadas durante todo o processo de desenvolvimento do software. 
% Algumas práticas do processo de desenvolvimento ágil merecem ser destacadas quando nos referimos ao gap entre as atividades de 
% Elicitação de Requisitos e Verificação e Validação, tais como:

% \begin{itemize}
%   \item \textit{Face-to-face communication} (Comunicação cara-a-cara) entre a equipe de desenvolvimento e os clientes do projeto,
%     minimizando a burocracia e a formalidade na comunicação entre as duas partes.

%   \item \textit{Customer involvement and interaction} (Envolvimento e interação do cliente) devido a necessidade que a abordagem
%     tem de uma interação rotineira e participativa.

%   \item \textit{User stories} (Histórias de Usuário): Técnica utilizada como uma forma de registrar os requisitos do sistema
%     de maneira informal, eficiente e clara para as todas as partes envolvidas no projeto.

%   \item \textit{Requirement prioritization} (Priorização dos requisitos): Os requisitos do sistema são priorizados continuamente
%     a partir da interação do cliente, focando o processo de desenvolvimento no valor de negócio do projeto, agregando valor
%     ao processo de desenvolvimento. Entretanto, \cite{inayat2015systematic} levanta o desafio de ponderar o processo de priorização,
%     levando em consideração, além do valor de negócio, questões técnicas de desenvolvimento, como escalabilidade, por exemplo.

%   \item \textit{Change management} (Gerenciamento de mudanças): A comunicação cara-a-cara, adoata em abordagens ágeis de
%     desenvolvimento, facilita o processo de gerenciamento de mudanças, já que os requisitos estão sendo acompanhados de perto
%     pelo cliente. Além disso, a transparência e interatividade gerada por abordagens ágeis faz com que o cliente possa aprimorar os requisitos
%     a todo momento, sendo facilitada por práticas como a apresentada a seguir.

%   \item \textit{Prototyping} (Prototipagem): A prática de prototipagem minimiza ainda mais o \textit{gap} entre a equipe
%     de desenvolvimento e o cliente do projeto. Além de agregar valor durante o processo de desenvolvimento, a prototipagem
%     facilita o processo de Validação do sistema, garantindo que o sistema desenvolvido está de acordo com as necessidades do cliente.

%   \item \textit{Testing before coding} (Teste antes da implementação): Diversos benefícios são levantados pela comunidade de desenvolvimento
%     em relação a estratégias de desenvolvimento que seguem o fluxo TDD, ou \textit{Test-Driven Development}. Entre os benefícios, temos
%     a garantia de um código mais conciso e menos acoplado, manutenível e com testes menos enviesados, como afirmam \cite{beck2003test},
%     \cite{crispin2009agile}, \cite{delamaro2017introduccao} e \cite{BDD_dan_north}.

%   \item \textit{Review meetings and acceptance tests} (Métricas de revisão e testes de aceitação): são maneiras de garantir que
%     as \textit{features} já implementadas estão e continuam de acordo com as necessidades do cliente no decorrer do processo
%     de desenvolvimento. Os testes de aceitação são como testes unitários, porém verificando a corretude da história de usuário \cite{inayat2015systematic}.

% \end{itemize}

% De acordo com o apresentado acima, metodologias ágeis de desenvolvimento buscam, entre outras coisas, minimizar o \textit{gap} entre
% as atividades de Elicitação de Requisitos e Verificação e Validação. Entre as abordagens ágeis mais utilizadas com este objetivo,
% destaca-se, neste trabalho, a abordagem de \textit{Behavior Driven Development} (BDD), ou Desenvolvimento Dirigido por Comportamento,
% proposta por \cite{BDD_dan_north}. Nesta abordagem, que será explicada em detalhes na Seção \ref{sec:bdd}, os requisitos do sistema
% e os casos de teste se unem no mesmo artefato, fazendo com que o requisito possa ser verificado de maneira automatizada,
% possibilitando uma interface ubíqua entre todos os \textit{stakeholders} do sistema em desenvolvimento. 
