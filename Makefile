all:
	pdflatex monografia
	bibtex monografia
	makeglossaries monografia
	pdflatex monografia
	pdflatex monografia
clean:
	@echo "Limpando arquivos temporários..."
	@rm -rf *.acn *.acr *.alg *.aux *.bbl *.blg *.brf *.glg *.glo *.gls *.glsdefs *.ist *.lof *.log *.lot *.out *.toc *.pdf
